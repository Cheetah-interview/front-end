import { Component } from '@angular/core';
import { NbDialogService, NbWindowService } from '@nebular/theme';
import { LocalDataSource } from 'ng2-smart-table';

import { CrudHttpService } from '../../../services/crud-http.service';
import { DialogEncryptComponent } from '../../modal-overlays/dialog/dialog-encrypt/dialog-encrypt.component';
import { DialogNamePromptComponent } from '../../modal-overlays/dialog/dialog-name-prompt/dialog-name-prompt.component';

@Component({
  selector: 'ngx-smart-table',
  templateUrl: './smart-table.component.html',
  styleUrls: ['./smart-table.component.scss'],
})
export class SmartTableComponent {

  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave:true
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      _id: {
        title: 'ID',
        type: 'string',
      },
      brand: {
        title: 'Brand',
        type: 'string',
      },
      model: {
        title: 'Model',
        type: 'string',
      },
      serial: {
        title: 'Serial',
        type: 'string',
      },
      email: {
        title: 'E-mail',
        type: 'string',
      },
    },
    actions: {
      custom: [
        {
          name: 'decrypt',
          title: '<i class="ion-document" title="YourAction"></i>'
        }
      ],
      add: false,
      position: "right",
      search: false
    },
  };

  source: LocalDataSource = new LocalDataSource();
  recordList : any = [];
  names : any = []

  constructor(private windowService: NbWindowService, private crudHttpService: CrudHttpService, private dialogService: NbDialogService) {
  }

  ngOnInit(): void {
    this.showAll();
    this.source.load(this.recordList);
  }

  onCustomAction(event){

    this.crudHttpService.showDecrypt(event.data._id).subscribe((response) => {
      const decryptData = response;
      this.dialogService.open(DialogEncryptComponent, {context: {decryptData: decryptData}})
        .onClose.subscribe(data => this.createRecord(data));
    },(error => {}));
  }

  open3() {
    this.dialogService.open(DialogNamePromptComponent)
      .onClose.subscribe(data => this.createRecord(data));
  }

  showAll(){
    this.crudHttpService.showAll().subscribe((response) => {
      this.recordList = response;
      this.source.load(this.recordList);
    },(error => {}));
  }

  createRecord(data:any){
    this.crudHttpService.create(data).subscribe((response) => {
      this.showAll();
    },(error => {console.log(error)}));
  }

  updateRecord(event:any){
    console.log("---**--> ", event.data._id)
    this.crudHttpService.update(event.data._id,event.newData).subscribe((response)=>{
      this.showAll();
    },(error => {console.log(error)}));
  }

  deleteRecord(event:any){
    console.log("........: ", event.data)
    this.crudHttpService.delete(event.data._id).subscribe((response)=>{
      this.showAll();
    },(error => {console.log("error: ", error)}));
  }

  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      this.deleteRecord(event)
    } else {
      event.confirm.reject();
    }
  }
}
