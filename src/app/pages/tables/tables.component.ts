import { Component } from '@angular/core';
import { CrudHttpService } from '../../services/crud-http.service';

@Component({
  selector: 'ngx-tables',
  template: `<router-outlet></router-outlet>`,
})
export class TablesComponent {

  recordList : any = [];

  constructor(private crudHttpService: CrudHttpService) {
  }

  ngOnInit(): void {
    this.showAll();
  }

  showAll(){
    this.crudHttpService.showAll().subscribe((response) => {
      this.recordList = response;
    },(error => {}));
  }

  createRecord(data:any){
    this.crudHttpService.create(data).subscribe((response) => {
      this.showAll();
    },(error => {}));
  }

  updateRecord(data:any){
    this.crudHttpService.update(data.id,data).subscribe((response)=>{
      this.showAll();
    },(error => {}));
  }

  deleteRecord(id:any){
    this.crudHttpService.delete(id).subscribe((response)=>{
      this.showAll();
    },(error => {}));
  }
}
